import React from 'react'
import { Container, Form } from 'react-bootstrap'

const RegisterScreen = () => {
  return (
    <Container>
      <Form>
        <Form.Group>
          <Form.Label>Email address</Form.Label>
          <Form.Control type='email' placeholder='Enter email' />
        </Form.Group>
        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control type='password' placeholder='Password' />
        </Form.Group>
        <Form.Group>
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control type='password' placeholder='Confirm Password' />
        </Form.Group>
      </Form>
    </Container>
  )
}

export default RegisterScreen
