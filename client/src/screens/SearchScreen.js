import React from 'react'
import { Container, Form } from 'react-bootstrap'

const SearchScreen = () => {
  return (
    <Container>
      <h3>This is search page</h3>
      <Form>
        <Form.Group>
          <Form.Label>Username</Form.Label>
          <Form.Control type='text' placeholder='Enter username' />
        </Form.Group>
        <Form.Group>
          <Form.Label>Email</Form.Label>
          <Form.Control type='text' placeholder='Enter email' />
        </Form.Group>
        <Form.Group>
          <Form.Label>Experience</Form.Label>
          <Form.Control type='text' placeholder='Enter experience' />
        </Form.Group>
        <Form.Group>
          <Form.Label>Level</Form.Label>
          <Form.Control type='text' placeholder='Enter level' />
        </Form.Group>
      </Form>
    </Container>
  )
}

export default SearchScreen
