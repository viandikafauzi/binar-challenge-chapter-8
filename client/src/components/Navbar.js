import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'

const Navbars = () => {
  return (
    <Navbar bg='light' variant='light' collapseOnSelect>
      <Container>
        <Navbar.Brand href='/'>Binar Academy</Navbar.Brand>
        <Nav>
          <Nav.Link href='#docs'>Docs</Nav.Link>
        </Nav>
        <Nav className='ml-auto'>
          <Nav.Link href='/search'>
            <i className='fas fa-search'></i> Search
          </Nav.Link>
          <Nav.Link href='/register'>
            <i className='fas fa-user-plus'></i> Register
          </Nav.Link>
          <Nav.Link href='/login'>
            <i className='fas fa-user'></i> Login
          </Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  )
}

export default Navbars
